package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IRepository;
import com.tsc.apogasiy.tm.api.service.IService;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyIdException;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyIndexException;
import main.java.com.tsc.apogasiy.tm.exception.entity.EntityNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.system.IndexIncorrectException;
import main.java.com.tsc.apogasiy.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    public void add(final E entity) {
        if (entity == null)
            throw new EntityNotFoundException();
        repository.add(entity);
    }

    public void remove(final E entity) {
        if (entity == null)
            throw new EntityNotFoundException();
        repository.remove(entity);
    }

    public List<E> findAll() {
        return repository.findAll();
    }

    public List<E> findAll(Comparator<E> comparator) {
        if (comparator == null)
            return Collections.emptyList();
        else
            return repository.findAll(comparator);
    }

    public void clear() {
        repository.clear();
    }

    public boolean isEmpty() {
        return repository.isEmpty();
    }

    public E findById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return repository.findById(id);
    }

    public E findByIndex(Integer index) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    public boolean existsById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return repository.existsById(id);
    }

    public boolean existsByIndex(Integer index) {
        return repository.existsByIndex(index);
    }

    @Override
    public E removeById(String id) {
        if (id == null) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E removeByIndex(final Integer index) {
        if (index == null) throw new EmptyIndexException();
        return repository.removeByIndex(index);
    }

}
