package main.java.com.tsc.apogasiy.tm.command;

import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyNameException;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand{

    protected void showTask(Task task) {
        if (task == null)
            throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

    protected Task add(final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return new Task(name, description);
    }

}
