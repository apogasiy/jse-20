package main.java.com.tsc.apogasiy.tm.command.task;

import main.java.com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Task;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-finish-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Finish task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskService().finishByIndex(userId, index);
    }

}
