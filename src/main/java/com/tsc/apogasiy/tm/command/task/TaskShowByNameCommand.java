package main.java.com.tsc.apogasiy.tm.command.task;

import main.java.com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Task;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-show-by-name";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show task by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findByName(userId, name);
        if (task == null)
            throw new TaskNotFoundException();
        showTask(task);
    }

}
