package main.java.com.tsc.apogasiy.tm.exception.system;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied!");
    }
}
