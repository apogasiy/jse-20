package main.java.com.tsc.apogasiy.tm.exception.empty;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyFirstNameException extends AbstractException {

    public EmptyFirstNameException() {
        super("Error! First name is empty!");
    }

}
