package main.java.com.tsc.apogasiy.tm.api.service;

import com.tsc.apogasiy.tm.api.service.IAuthService;
import com.tsc.apogasiy.tm.api.service.IUserService;

public interface IServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

}
