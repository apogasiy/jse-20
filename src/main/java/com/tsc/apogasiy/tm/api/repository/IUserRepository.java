package main.java.com.tsc.apogasiy.tm.api.repository;

import main.java.com.tsc.apogasiy.tm.model.User;

import java.util.Comparator;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    List<User> findAll();

    List<User> findAll(Comparator<User> comparator);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

    boolean userExistsByLogin(String login);

    boolean userExistsByEmail(String email);

}
