package main.java.com.tsc.apogasiy.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
