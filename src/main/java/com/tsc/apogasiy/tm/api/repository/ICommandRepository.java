package main.java.com.tsc.apogasiy.tm.api.repository;

import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;
import main.java.com.tsc.apogasiy.tm.model.Command;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<String> getCommandNames();

    Collection<String> getArgNames();

    void add(AbstractCommand command);


}
