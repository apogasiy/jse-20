package main.java.com.tsc.apogasiy.tm.api.entity;

import java.util.Date;

public interface IHasStartDate {

    Date getStartDate();

    void setStartDate(Date startDate);

}
